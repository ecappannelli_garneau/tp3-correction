from flask import Blueprint, render_template, request

from dataModules.store import products, productLines

products = products.product()
productLines = productLines.productLine()

productLine = Blueprint('productLine', __name__, url_prefix='/productLine', template_folder='templates')


@productLine.route('/', methods=['GET'])
def list_product_line():
    product_line = request.args.get('productLine', default='', type=str)
    page = request.args.get('page', default=1, type=int)

    if page <= 0:
        page = 1

    return render_template('productList.html',
                           data=products.search_product_for_productLine(product_line, page)
                           )
