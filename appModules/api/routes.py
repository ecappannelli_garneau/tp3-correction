from flask import Blueprint, render_template, request, redirect, session, g, jsonify
import simplejson as json
from dataModules.store import products

products = products.product()

api = Blueprint('api', __name__,
                url_prefix='/api',
                )


@api.route('/')
def default():
    return 'Hello world'


@api.route('/getQuote', methods=['POST'])
def get_quote():
    #
    # On reçoit un contenu de panier directement au format Json grâce au contentType 'application/json' dans l'appel Ajax:
    #
    # print(json.dumps(request.json))
    dic_cart = request.json
    # print(dic_cart.keys())
    # for key in dic_cart:
    #     print(dic_cart[key])

    resp = products.get_quote(dic_cart)
    # print(resp)

    return json.dumps(resp)


@api.route('/placeOrder', methods=['POST'])
def place_order():
    if g.user['user_name'] == 'Guest':
        return json.dumps({'error': True, 'message': 'Utilisateur inconnu'})

    return json.dumps(products.place_order(request.json, g.user['user_id']))
