from flask import Blueprint, render_template, request, redirect, session, g, abort
from dataModules.users import users

orders = Blueprint('orders', __name__,
                   url_prefix='/orders',
                   template_folder='templates',
                   # Fait référence au sous répertoire 'static' de mon module blueprint :
                   static_folder='static',
                   # Permet de préciser le répertoire virtuel qui sera créé (/auth/scripts/...) :
                   static_url_path='scripts'
                   )


@orders.route('/<userId>')
def default(userId):
    #
    # Test si l'utilisateur a la droit !
    #
    if g.user['user_name'] == 'Guest':
        return abort(403)
    #
    #
    #

    return render_template('orders.html')
