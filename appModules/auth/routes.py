from flask import Blueprint, render_template, request, redirect, session, g
from dataModules.users import users

user = users.user()

auth = Blueprint('auth', __name__,
                 url_prefix='/auth',
                 template_folder='templates',
                 # Fait référence au sous répertoire 'static' de mon module blueprint :
                 static_folder='static',
                 # Permet de préciser le répertoire virtuel qui sera créé (/auth/scripts/...) :
                 static_url_path='scripts'
                 )


@auth.route('/')
def default():
    return redirect('/auth/login')


@auth.route('/logout')
def logout():
    user_name = g.user['user_name']
    session.pop('user_id')
    session.pop('is_employee')
    session.pop('user_name')
    session['message'] = f"A bientôt {user_name} !"
    return redirect('/')


@auth.route('/login', methods=['POST', 'GET'])
def login():
    error = False
    form_details = {}
    if request.method == 'POST':
        form_details = request.form
        login_info = user.login(request)
        if not login_info['error']:
            user_name = request.form['username']

            session['is_employee'] = login_info['is_employee']
            session['user_name'] = login_info['user_name']
            session['user_id'] = login_info['user_id']

            session['message'] = f"Bienvenue {user_name} !"

            return redirect('/')
        else:
            error = login_info['errors']

    return render_template('login.html', content={'errors': error, "details": form_details})


@auth.route('/createAccount', methods=['POST', 'GET'])
def create_fnc():
    error = False
    formDetails = {}
    if request.method == 'POST':
        formDetails = request.form
        error = user.create(request)
        if not error:
            user_name = request.form['username']
            print('!! OK')
            session['is_employee'] = False
            # session['user_id'] = login_info['user_id'] << Le user_id est ajouté dans la methode create (limite)
            session['user_name'] = user_name
            session['message'] = f"Le compte {user_name} a été créé avec succès."
            return redirect('/')

    return render_template('createAccount.html', content={'errors': error, "details": formDetails})
