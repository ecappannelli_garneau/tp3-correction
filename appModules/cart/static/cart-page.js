"use strict";

//
// Technique #2 : Permet de générer la page du contenu du panier à partir d'ajax+jQuery
//
$(document).ready(function () {

    //
    //
    //

    function getLocalCart() {
        var localCart = {};
        if (localStorage.getItem("cart") !== null) {
            localCart = JSON.parse(localStorage.getItem('cart'));
        }
        return localCart;
    }

    //
    // Démarrage de la page : on va aller chercher les infos les plus à jour via l'API
    //
    $.ajax({
        type: "POST",
        url: '/api/getQuote',
        dataType: 'json',
        contentType: 'application/json',
        async: false,
        data: JSON.stringify(getLocalCart()),
        success: function (data) {

            var localCart = getLocalCart();

            if (data.error) {

                $('#shoppingCartError').text(data.message).fadeIn();
                console.log(data);
                // alert('Erreur :' + data.message);

            } else {

                //
                // Todo : générer le tableau des produits qui ne sont pas en stock
                //

                var $table = $('<div/>').addClass('container');
                // $('#cartInStockTable').append('<div>');

                var $row = $('<div/>').addClass('row');
                $row.append('<div class="col-md-9">Description</div>');
                $row.append('<div class="col-md-1"><p class="text-end">Prix</p></div>');
                $row.append('<div class="col-md-1"><p class="text-end">Quantité</p></div>');
                $row.append('<div class="col-md-1"><p class="text-end">Total</p></div>');
                $table.append($row);

                Object.keys(data['inStock']).forEach(function (aKey) {
                    var $row = $('<div/>').addClass('row');
                    $row.append('<div class="col-md-9">' + data['inStock'][aKey]['productName'] + '</div>');
                    $row.append('<div class="col-md-1"><p class="text-end">' + data['inStock'][aKey]['price'] + '$</p></div>');
                    $row.append('<div class="col-md-1"><p class="text-end">' + data['inStock'][aKey]['quantity'] + '</p></div>');
                    $row.append('<div class="col-md-1"><p class="text-end">' + data['inStock'][aKey]['quantity'] * data['inStock'][aKey]['price'] + '$</p></div>');
                    $table.append($row);

                    //
                    // On en profite pour ajouter le prix dans notre Cart local...
                    //
                    localCart[aKey]['price'] = data['inStock'][aKey]['price'];

                });

                var $row = $('<div/>').addClass('row');
                $row.append('<div class="col-md-9"></div>');
                $row.append('<div class="col-md-2"><p class="text-end fw-bolder">Total :</p></div>');
                $row.append('<div class="col-md-1"><p class="text-end fw-bolder">' + data['totalPrice'] + '$</p></div>');
                $table.append($row);

                $('#cartInStockTable').append($table);
                $('#quoteReady').fadeIn();


            }

            //
            // Sauvegarde du cart avec les prix obtenus (repassé ensuite pour commander)
            //
            localStorage.setItem('cart', JSON.stringify(localCart));

        },
        error: function (e) {
            console.log(e);
            alert('Error Ajax');
        }

    })

    //
    // Gestion du bouton commander
    //
    $('#btnOrder').bind("click", function () {

        $('#shoppingCartError').hide();
        $('#btnOrder').prop("disabled", true);

        $.ajax({
            type: "POST",
            url: '/api/placeOrder',
            dataType: 'json',
            contentType: 'application/json',
            async: false,
            data: JSON.stringify(getLocalCart()),
            success: function (data) {

                $('#btnOrder').prop("disabled", false);

                if (data.error) {

                    $('#shoppingCartError').text(data.message).fadeIn();
                    console.log(data);

                } else {

                    //
                    // Succès !
                    //
                    $('#orderNumber').text(data['orderDetails']['orderNumber']);
                    $('#shoppingOrderOk').fadeIn();
                    //
                    // On masque le bouton commander pour ne pas double-commander...
                    //
                    $('#btnOrder').fadeOut();

                    //
                    // On vide aussi le contenu du panier...
                    //
                    localStorage.setItem('cart', JSON.stringify({}));

                }

            }

        });

    });

});