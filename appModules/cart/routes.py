from flask import Blueprint, render_template, request, redirect, session, g, abort
from dataModules.users import users

cart = Blueprint('cart', __name__,
                 url_prefix='/cart',
                 template_folder='templates',
                 # Fait référence au sous répertoire 'static' de mon module blueprint :
                 static_folder='static',
                 # Permet de préciser le répertoire virtuel qui sera créé (/auth/scripts/...) :
                 static_url_path='scripts'
                 )


@cart.route('/')
def default():

    return redirect('/cart/view')


#
# Affiche la page Cart
#   Mais les données sont générées côté client avec du Javascript
#   La page appelera l'api '/api/getQuote' pour avoir un 'devis' en fonction des
#   produits commandés, leur prix au moment ou il est généré, et la quantité commandée.
#
@cart.route('/view')
def view_cart():
    #
    # Test si l'utilisateur a la droit !
    #
    if g.user['user_name'] == 'Guest':
        return abort(403)
    #
    #
    #

    return render_template('cartPage.html')
