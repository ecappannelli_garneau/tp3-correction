from flask import Flask, render_template, request, session, g
from database import get_connection, close_connection

from dataModules.store import products, productLines

from appModules.products.routes import product
from appModules.productsLine.routes import productLine
from appModules.auth.routes import auth
from appModules.cart.routes import cart
from appModules.orders.routes import orders
from appModules.api.routes import api

app = Flask(__name__)
# Pour pouvoir utiliser les sessions...
app.secret_key = 'any random string very random....'

#
# Blueprints
#
# Attention 'product' (le module blueprint) et pas 'products' (la classe)
app.register_blueprint(product)
app.register_blueprint(productLine)
app.register_blueprint(auth)
app.register_blueprint(cart)
app.register_blueprint(orders)
app.register_blueprint(api)

#
# Classes-Models
#
products = products.product()
productLines = productLines.productLine()


@app.before_request
def before_request():
    #
    # Test de session:
    #
    get_connection()

    if session.get('user_id'):
        user = {
            'user_name': session['user_name'],
            'user_id': session['user_id'],
            'is_employee': session['is_employee']
        }
    else:
        user = {'user_name': 'Guest', 'is_employee': False}

    g.user = user

    #
    # Regarde si il n'y a pas un cookie avec un message ;)
    #
    if session.get('message'):
        g.message = session.get('message')
        session.pop('message')


@app.after_request
def after_request(response):
    return close_connection(response)


@app.route('/')
def home():
    return render_template('index.html',
                           content={
                               'top_5_posts': products.top_5_products(),
                               'product_lines': productLines.get_product_lines()
                           }
                           )


@app.route('/search', methods=['GET'])
def search_product():
    keyword = request.args.get('search', default='', type=str)
    page = request.args.get('page', default=1, type=int)

    if page <= 0:
        page = 1

    return render_template('productList.html',
                           data=products.search_product(keyword, page)
                           )


if __name__ == '__main__':
    app.run(debug=True)
