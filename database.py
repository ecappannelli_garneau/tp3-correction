from flask import g
import mysql.connector


def get_connection():
    try:
        connection = mysql.connector.connect(
            host='localhost',
            database='classicmodels',
            user='root',
            password='',
            autocommit=False
        )

        if connection.is_connected():
            g.connection = connection

    except mysql.connector.Error as e:
        print("Error connection database", e)
        raise e


def close_connection(response):
    if g.connection is not None:
        print('closing connection')
        g.connection.close()
    return response
