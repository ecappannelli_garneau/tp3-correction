from flask import g


class productLine():

    def get_product_lines(self):
        try:
            cursor = g.connection.cursor(dictionary=True)
            sql_select_query = f"""
                Select pl.productLine, pl.textDescription, count(p.productCode) nb
                from productLines pl
                join products p on p.productLine = pl.productLine
                group by pl.productLine
            """
            params = ()
            cursor.execute(sql_select_query, params)
            records = cursor.fetchall()
            cursor.close()

            return records

        except Exception as e:
            print("Error reading data from MySQL table", e)
